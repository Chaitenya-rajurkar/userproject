import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserdetailComponent } from './Compoent/userdetail/userdetail.component';
import { UserprofileComponent } from './Compoent/userprofile/userprofile.component';

const routes: Routes = [
  {path:'',component:UserdetailComponent},
  {path:'login',component:UserprofileComponent,data:[]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
