import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalobjectsService } from 'src/app/services/globalobjects.service';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
   dataOfUser:any=[];
   getprofiledata:any=[];
  constructor(public route: ActivatedRoute,private router:Router,private globalObj:GlobalobjectsService) {
    try {
      this.dataOfUser = this.router.getCurrentNavigation().extras.state.profileDetails;
      
    } catch (error) {
      
    }
   }

  ngOnInit(): void {
  
      this.globalObj.getProfile(this.dataOfUser.url).then(data => {
        this.getprofiledata = data;
        console.log(this.getprofiledata)
      }).catch(err => {
  
        console.log(err);
      });
  }
}
