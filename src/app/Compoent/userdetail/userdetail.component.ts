import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalobjectsService } from 'src/app/services/globalobjects.service';
declare var google;
@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit {
  result: any=[];
  user: any;
  grapharray:any=[];
  constructor(public globalObj: GlobalobjectsService,public router:Router) { }

  ngOnInit(): void {
   
  }
getFollowers(){
  this.globalObj.getAllwithParam(this.result.items.followers_url).then(data => {
    this.result = data;
    console.log(this.result)
    let i:number=0;

  }).catch(err => {

    console.log(err);
  });
}
  getuserData() {
    this.globalObj.getAllwithParam(this.user).then(data => {
      this.result = data;
      console.log(this.result)
      let i:number=0;
 
    }).catch(err => {

      console.log(err);
    });

  }
  gotoLoin(event){
console.log(event);
this.router.navigate(['/login'], { 
  state: { profileDetails: event }
});
  }

  drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Element", "Density", { role: "style" } ],
      ["Copper", 8.94, "#b87333"],
      ["Silver", 10.49, "silver"],
      ["Gold", 19.30, "gold"],
      ["Platinum", 21.45, "color: #e5e4e2"]
    ]);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
                     { calc: "stringify",
                       sourceColumn: 1,
                       type: "string",
                       role: "annotation" },
                     2]);

    var options = {
      title: "Density of Precious Metals, in g/cm^3",
      width: 600,
      height: 400,
      bar: {groupWidth: "95%"},
      legend: { position: "none" },
    };
    // var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    // chart.draw(view, options);

    setTimeout(() => {
      let element=document.getElementById("chart_div");
      var chart = new google.visualization.BarChart(element);
      chart.draw(view, options);
     }
     , 2000); 
}

ngAfterViewInit(){
  this.drawChart();
}
}
