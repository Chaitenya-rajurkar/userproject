import { TestBed } from '@angular/core/testing';

import { GlobalobjectsService } from './globalobjects.service';

describe('GlobalobjectsService', () => {
  let service: GlobalobjectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalobjectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
