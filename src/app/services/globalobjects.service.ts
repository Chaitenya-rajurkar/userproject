import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GlobalobjectsService {
  
  dataArray:any=[];
  constructor(private http:HttpClient) { }

  getAllwithParam(q){
    return new Promise((resolve, reject) => {
    let urlwithparam="https://api.github.com/search/users?q="+q;
    this.http.get(urlwithparam).subscribe(data=>{
       this.dataArray=data;
       console.log(this.dataArray)  
       resolve(data);
    }, err => {
      reject(err);
   }) 
  })   

  }

  getProfile(url){
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(data=>{
        this.dataArray=data;
        console.log(this.dataArray)  
        resolve(data);
     }, err => {
       reject(err);
    }) 
   })   
  }
}
