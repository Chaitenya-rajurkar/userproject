import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserdetailComponent } from './Compoent/userdetail/userdetail.component';
import { GlobalobjectsService } from './services/globalobjects.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserprofileComponent } from './Compoent/userprofile/userprofile.component';

@NgModule({
  declarations: [
    AppComponent,
    UserdetailComponent,
    UserprofileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule,
    
  ],
  providers: [GlobalobjectsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
